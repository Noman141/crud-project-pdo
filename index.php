<?php
include 'lib/Session.php';
include 'inc/header.php' ;
include 'lib/Database.php';
?>
<?php
Session::init();
$msg = Session::get('msg');
if (!empty($msg)){
    echo $msg;
    Session::unnset();
}
?>
  	<!-- Card Wider -->
	<div class="card card-cascade wider">
	  <!-- Card content -->
	  <div class="card-body card-body-cascade text-center">
	    <!-- Title -->
	    <h2 class="card-title"><strong>Student List</strong></h2>
	    <!-- Subtitle -->
	    <div class="d-flex justify-content-between">
	    	<h5 class="ml-5 pb-2"><strong>Student Data</strong></h5>
	    	<h5 class="mr-5 pb-2"><a class="btn btn-info" href="addstudent.php">Add Suudent</a></h5>
	    </div>
	    
	    <!--Table-->
		<table class="table table-striped">

		  <!--Table head-->
		  <thead>
		    <tr>
		      <th>Serial</th>
		      <th>Name</th>
		      <th>Email</th>
		      <th>Phone Number</th>
		      <th>Age</th>
		      <th>Action</th>
		    </tr>
		  </thead>
		  <!--Table head-->


		  <!--Table body-->
		  <tbody>
          <?php
          $db = new Database();
          $table = "tbl_student";
          $order_by = array('order_by'=> 'id DESC');
          /*
          $selectcond = array('select' => 'name');
          $wherecond = array(
                  'where' => array('id' => '2', 'email' => 'pia@gmail.com'),
                   'return_type' => 'single'
          );
          $limit = array('start' => '2','limit' => '4');
          $limit = array('limit'=> '4');
          */
          $studentData = $db->select($table,$order_by);

          if (!empty($studentData)){
              $i = 0;
              foreach ($studentData as $data){
                  $i++;?>
		    <tr class="table">
		      <th scope="row"><?php echo $i;?></th>
		      <td><?php echo $data['name']?></td>
		      <td><?php echo $data['email']?></td>
		      <td><?php echo $data['phone']?></td>
		      <td><?php echo $data['age']?></td>
		      <td>
		      	<a class="btn btn-default" href="editstudent.php?id=<?php echo $data['id']?>">Edit</a>
		      	<a class="btn btn-danger" href="lib/process_student.php?action=delete&id=<?php echo $data['id']?>" onclick="return confirm('Are You Sure To Delete!')">Delete</a>
		      </td>
		    </tr>
          <?php } }else{ ?>
              <tr><td colspan="5"><h2 class="alert alert-danger">No Student Data Found...!</h2></td></tr>
          <?php }?>
		  </tbody>
		  <!--Table body-->


		</table>
		<!--Table-->

	  </div>

	

  <?php include 'inc/footer.php';?>
