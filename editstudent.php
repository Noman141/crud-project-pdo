<?php
include 'inc/header.php';
include 'lib/Database.php';
$db = new Database();
?>
<!-- Card Wider -->
	<div class="card card-cascade wider">
	  <!-- Card content -->
	  <div>
	  	<!-- Subtitle -->
	    <div class="d-flex justify-content-between">
	    	<h3 class="ml-5 py-2"><strong>Update Student</strong></h3>
	    	<h5><a class="btn btn-info" href="index.php">Back</a></h5>
	    </div>
	  	    
	  <div class="card-body card-body-cascade text-center">
	    <!-- Title -->
	  <?php
        if (isset($_GET['id']) && !empty($_GET['id'])){
            $id = $_GET['id'];
            $table = "tbl_student";
            $wherecond = array(
                'where' => array('id' => $id),
                'return_type' => 'single'
            );
            $getData = $db->select($table, $wherecond);
        }

        if (!empty($getData)){?>

	    <!-- Horizontal material form -->
		<form action="lib/process_student.php" method="post">
		  <!-- Grid row -->
		  <div class="form-group row">
		    <!-- Material input -->
		    <label for="name" class="col-sm-2 col-form-label">Student Name</label>
		    <div class="col-sm-10">
		      <div class="md-form mt-0">
		        <input type="text" name="name" class="form-control" id="name" value="<?php echo $getData['name'];?>">
		      </div>
		    </div>
		  </div>
		  <!-- Grid row -->

		  <!-- Grid row -->
		  <div class="form-group row">
		    <!-- Material input -->
		    <label for="email" class="col-sm-2 col-form-label">Email</label>
		    <div class="col-sm-10">
		      <div class="md-form mt-0">
		        <input type="text" class="form-control" name="email" id="email" value="<?php echo $getData['email'];?>">
		      </div>
		    </div>
		  </div>
		  <!-- Grid row -->

		  <!-- Grid row -->
		  <div class="form-group row">
		    <!-- Material input -->
		    <label for="phone" class="col-sm-2 col-form-label">Phone Number</label>
		    <div class="col-sm-10">
		      <div class="md-form mt-0">
		        <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $getData['phone'];?>">
		      </div>
		    </div>
		  </div>
		  <!-- Grid row -->

		  <!-- Grid row -->
		  <div class="form-group row">
		    <!-- Material input -->
		    <label for="age" class="col-sm-2 col-form-label">Age</label>
		    <div class="col-sm-10">
		      <div class="md-form mt-0">
		        <input type="text" class="form-control" name="age" id="age" value="<?php echo $getData['age'];?>">
		      </div>
		    </div>
		  </div>
		  <!-- Grid row -->

		  <!-- Grid row -->
		  <div class="form-group row">
		    <div class="col-sm-10">
		      <input type="hidden" name="id" value="<?php echo $getData['id'];?>">
		      <input type="hidden" name="action" value="edit">
		      <button type="submit" class="btn btn-primary btn-md">update student</button>
		    </div>
		  </div>
		  <!-- Grid row -->
		</form>
		<!-- Horizontal material form -->
		<?php }else{ ?>
           <h2 class="alert alert-danger">Data Not Found!</h2>
        <?php }?>
	 </div>
	</div>





<?php include 'inc/footer.php' ?>