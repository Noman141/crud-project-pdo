<?php
include 'Database.php';
include 'Session.php';
Session::init();
$db = new Database();
$table = 'tbl_student';

if (isset($_REQUEST['action']) && !empty($_REQUEST['action'])){
    if ($_REQUEST['action'] == 'add'){
        $studentData = array(
          'name'  => $_POST['name'],
          'email' => $_POST['email'],
          'phone' => $_POST['phone'],
          'age'   => $_POST['age']
        );
        $insert = $db->insert($table,$studentData);
        if ($insert){
            $msg = "<h2 class='alert alert-success text-center'>Data Insert Successfully</h2>";
        }else{
            $msg = "<h2 class='alert alert-danger text-center'>Data Not Insert</h2>";
        }
        Session::set('msg', $msg);
        $home_url = '../index.php';
        header('Location:'.$home_url);
    }elseif ($_REQUEST['action'] == 'edit'){
            $id = $_POST['id'];
            if (!empty($id)){
                $studentData = array(
                    'name'  => $_POST['name'],
                    'email' => $_POST['email'],
                    'phone' => $_POST['phone'],
                    'age'   => $_POST['age']
                );
                $table = "tbl_student";
                $condition = array('id'=>$id);
                $update = $db->update($table,$studentData,$condition);
                if ($update){
                    $msg = "<h2 class='alert alert-success text-center'>Data Updated Successfully</h2>";
                }else{
                    $msg = "<h2 class='alert alert-danger text-center'>Data Not Updated</h2>";
                }
                Session::set('msg', $msg);
                $home_url = '../index.php';
                header('Location:'.$home_url);
            }
    }elseif ($_REQUEST['action'] == 'delete'){
        $id = $_GET['id'];
        if (!empty($id)){
            $table = "tbl_student";
            $condition = array('id'=>$id);
            $delete = $db->delete($table,$condition);
            if ($delete){
                $msg = "<h2 class='alert alert-success text-center'>Data Deleted Successfully</h2>";
            }else{
                $msg = "<h2 class='alert alert-danger text-center'>Data Not Deleted</h2>";
            }
            Session::set('msg', $msg);
            $home_url = '../index.php';
            header('Location:'.$home_url);
        }
    }
}





?>