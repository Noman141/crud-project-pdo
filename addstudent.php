<?php include 'inc/header.php' ?>
<!-- Card Wider -->
	<div class="card card-cascade wider">
	  <!-- Card content -->
	  <div>
	  	<!-- Subtitle -->
	    <div class="d-flex justify-content-between">
	    	<h3 class="ml-5 py-2"><strong>Add Student</strong></h3>
	    	<h5><a class="btn btn-info" href="index.php">Back</a></h5>
	    </div>
	  	    
	  <div class="card-body card-body-cascade text-center">
	    <!-- Title -->
	    

	    <!-- Horizontal material form -->
		<form action="lib/process_student.php" method="post">
		  <!-- Grid row -->
		  <div class="form-group row">
		    <!-- Material input -->
		    <label for="name" class="col-sm-2 col-form-label">Student Name</label>
		    <div class="col-sm-10">
		      <div class="md-form mt-0">
		        <input type="text" class="form-control" name="name" id="name" placeholder="Student Name">
		      </div>
		    </div>
		  </div>
		  <!-- Grid row -->

		  <!-- Grid row -->
		  <div class="form-group row">
		    <!-- Material input -->
		    <label for="email" class="col-sm-2 col-form-label">Email</label>
		    <div class="col-sm-10">
		      <div class="md-form mt-0">
		        <input type="text" name="email" class="form-control" id="email" placeholder="Email">
		      </div>
		    </div>
		  </div>
		  <!-- Grid row -->

		  <!-- Grid row -->
		  <div class="form-group row">
		    <!-- Material input -->
		    <label for="phone" class="col-sm-2 col-form-label">Phone Number</label>
		    <div class="col-sm-10">
		      <div class="md-form mt-0">
		        <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone Number">
		      </div>
		    </div>
		  </div>
		  <!-- Grid row -->

		  <!-- Grid row -->
		  <div class="form-group row">
		    <!-- Material input -->
		    <label for="age" class="col-sm-2 col-form-label">Age</label>
		    <div class="col-sm-10">
		      <div class="md-form mt-0">
		        <input type="text" class="form-control" name="age" id="age" placeholder="Age">
		      </div>
		    </div>
		  </div>
		  <!-- Grid row -->

		  <!-- Grid row -->
		  <div class="form-group row">
		    <div class="col-sm-10">
		      <input type="hidden" name="action" value="add">
		      <button type="submit" class="btn btn-primary btn-md">Submit</button>
		    </div>
		  </div>
		  <!-- Grid row -->
		</form>
		<!-- Horizontal material form -->
	 </div>
	</div>





<?php include 'inc/footer.php' ?>