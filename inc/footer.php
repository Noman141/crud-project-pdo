  </div>
  <!-- Card Wider -->
  </div>
<div class="container">
  	<!-- Footer -->
	<footer class="page-footer font-small navbar-light blue lighten-4 ">

	  <!-- Copyright -->
	  <div class="footer-copyright text-center py-3">© 2018 Copyright:
	    <a href="https://twitter.com/AlNoman1416"> Noman</a>
	  </div>
	  <!-- Copyright -->

	</footer>
	<!-- Footer -->
  </div>
  <!-- /Start your project here-->

  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb.min.js"></script>
    <!-- custom js -->
    <script type="text/javascript" src="js/custom.js"></script>

</body>

</html>