<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>PHP Dynamic CRUD with OOP & PDO</title>
    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Emilys+Candy|Fontdiner+Swanky|Kaushan+Script|Mr+Dafoe" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <!-- <link href="css/style.css" rel="stylesheet"> -->
</head>


<body>

  <!-- Start your project here-->
  <div class="container">
    <nav class="navbar navbar-light blue lighten-4 d-flex justify-content-center">
       <h1>PHP Dynamic CRUD with OOP & PDO</h1>
    </nav>
  </div>
  
  <div class="container">